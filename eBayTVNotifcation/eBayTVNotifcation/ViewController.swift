//
//  ViewController.swift
//  eBayTVNotifcation
//
//  Created by Gabriel Kapach on 04/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var image: UIImageView!
   
    let socket = SocketIOClient(socketURL: "localHost:8080")
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        self.socket.connect()
        self.addHandlers()
    }
    func addHandlers(){
        
        self.socket.on("img") {[weak self] data, ack in
            if let img = data[0] as? UIImageView {
                self!.image = img
                
            }
        
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

