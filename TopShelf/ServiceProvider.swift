//
//  ServiceProvider.swift
//  TopShelf
//
//  Created by Gabriel Kapach on 07/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import Foundation
import TVServices

class ServiceProvider: NSObject, TVTopShelfProvider {

    override init() {
        super.init()
    }

    // MARK: - TVTopShelfProvider protocol

    var topShelfStyle: TVTopShelfContentStyle {
        // Return desired Top Shelf style.
        return .Sectioned
    }

    var topShelfItems: [TVContentItem] {
        let item1 = TVContentItem(contentIdentifier: TVContentIdentifier(identifier: "item1", container: nil)!)
        let item2 = TVContentItem(contentIdentifier: TVContentIdentifier(identifier: "item1", container: nil)!)
        let item3 = TVContentItem(contentIdentifier: TVContentIdentifier(identifier: "item1", container: nil)!)
        
        item1?.imageShape = .ExtraWide
        item1?.imageURL = NSBundle.mainBundle().URLForResource("1.jpg", withExtension: nil)
        
        item2?.imageShape = .ExtraWide
        item2?.imageURL = NSBundle.mainBundle().URLForResource("2.jpg", withExtension: nil)
        
        item3?.imageShape = .ExtraWide
        item3?.imageURL = NSBundle.mainBundle().URLForResource("3.jpg", withExtension: nil)
        
        return [item1!, item2!, item3!]
    }

}

