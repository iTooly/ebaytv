//
//  ItemCollectionViewCell.swift
//  eBay
//
//  Created by Gabriel Kapach on 13/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
}
