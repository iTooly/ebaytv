//
//  ColorButton.swift
//  eBay
//
//  Created by Shay Dahan on 14.12.2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class ColorButton: FocusableButton {
    
    static var buttons : [ColorButton] = []
    var selectedF : Bool = false
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor(red: 0.1, green: 0.8, blue: 1, alpha: 1).CGColor
        ColorButton.buttons.append(self)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: "tapped:")
        tapRecognizer.allowedPressTypes = [NSNumber(integer: UIPressType.Select.rawValue)];
        self.addGestureRecognizer(tapRecognizer)
        
        radius = 15
        radiusF = 0
        scale = 0.4
        backColorF = false
    }
    
    func tapped(sender: UITapGestureRecognizer) {
        selectedF = !selectedF
        UIView.animateWithDuration(0.07) { () -> Void in
            if self.selectedF {
                self.layer.borderWidth = 5
            } else {
                self.layer.borderWidth = 0
            }
        }
    }
}
