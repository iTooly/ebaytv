//
//  CribePopupViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 05/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class CribePopupViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func GoCribe(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("GoHomeGame", object: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
