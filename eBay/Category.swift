/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
A struct used throughout the sample to represent example data.
*/

import Foundation

struct Category: Equatable {
    
    // MARK: Properties
    
    let number: Int
    
    var title: String
    
    var imageURL: NSURL {
        let mainBundle = NSBundle.mainBundle()
        guard let imageURL = mainBundle.URLForResource(imageName, withExtension: nil) else { fatalError("Error determining local image URL.") }
        
        return imageURL
    }
    
    var imageName: String {
        return "\(title)_Logo"
    }
    
    /// A static sample set of Categorys.
    static var allCategorys: [Category] = {
        let catTitles: [String] = ["Entertainment Memorabilia", "Clothing, Shoes & Accessories", "Toys & Hobbies", "Sporting Goods", "Collectibles",  "Consumer Electronics", "Home & Garden", "Motors", "Other"]
            
        return (1...catTitles.count).map { Category(number: $0, title: "\(catTitles[$0 - 1])") }
    }()
}

// MARK: Equatable

func ==(lhs: Category, rhs: Category)-> Bool {
    // Two `DataItem`s are considered equal if their identifiers match.
    return lhs.title == rhs.title
}
