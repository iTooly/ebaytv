//
//  CribeHomeViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 16/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class CribeHomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "goScreen1", name: "DecisionsArrived", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Start(sender: AnyObject) {
        print(NSUserDefaults.standardUserDefaults().valueForKey("Auth"))
        if NSUserDefaults.standardUserDefaults().valueForKey("Auth") != nil {
            let socket = SocketIOClient(socketURL: "178.62.123.100:1967", opts: ["connectParams" : ["token" : NSUserDefaults.standardUserDefaults().valueForKey("Auth")!]])
            let newManager = GameManager(instance: socket)
            appDelegate().manager = newManager
        } else {
            
        }
    }
    
    func goScreen1() {
        self.performSegueWithIdentifier("Play", sender: self)
    }

    func appDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
}
