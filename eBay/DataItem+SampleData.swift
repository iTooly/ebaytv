/*
    Copyright (C) 2015 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    An extension of `DataItem` that provides a static array of sample `DataItem`s.
*/

import Foundation

extension DataItem {
    /// A static sample set of `DataItem`s.
    static var sampleItems: [DataItem] = {
        return Group.allGroups.flatMap { group -> [DataItem] in
            let itemCount: Int
            let catTitles: [String] = ["Consumer Electronics", "Collectibles", "Entertainment Memorabilia", "Clothing, Shoes & Accessories", "Sporting Goods", "Cribe"]
            let bigAdsTitles: [String] = ["BigAds", "BigAds", "BigAds" ]

            switch group {
                case .BigAds:
                    itemCount = 3
                return (1...itemCount).map { DataItem(group: group, number: $0, title: "\(bigAdsTitles[$0 - 1]) \($0)") }
                    
                case .Categories:
                    itemCount = 6
                return (1...itemCount).map { DataItem(group: group, number: $0, title: catTitles[$0 - 1]) }
            }

            
        }
    }()
}