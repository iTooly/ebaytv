/*
    Copyright (C) 2015 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    An extension of `DataItem` that provides string properties for a `DataItem`'s image name.
*/

import Foundation

extension DataItem {
    var imageName: String {
        return "\(title)_Logo"
    }
}
