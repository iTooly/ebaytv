//
//  CategoryNameCollectionViewCell.swift
//  eBay
//
//  Created by Gabriel Kapach on 16/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class CategoryNameCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    
}
