//
//  SearchViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 10/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var searchBtn: FocusableButton!
    @IBOutlet weak var search: UITextField!
    @IBOutlet weak var itemsCollection: UICollectionView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var products : [[String : JSON]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBtn.setImage(UIImage(named: "search_icon")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
        searchBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        searchBtn.backColorF = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func search(sender: AnyObject) {
        if search.text != nil {
            Alamofire.request(.POST, "http://178.62.123.100:8081/search", parameters: ["query" : search.text!, "PageNumber" : "1", "MaxEntries" : "25"], encoding: .JSON, headers: nil).responseJSON(completionHandler: { response -> Void in
                self.products = []
                if response.result.isSuccess {
                    if let result = JSON(response.result.value!).dictionary {
                        if result["success"]?.bool == true {
                            if let data = result["data"]?.dictionary {
                                if data["@count"]?.intValue > 0 {
                                    if let items = data["item"]!.array {
                                        for x in items {
                                            if let item = x.dictionary {
                                                self.products.append(item)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                self.itemsCollection.reloadData()
            })
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ItemCell", forIndexPath: indexPath) as! ItemCollectionViewCell
        let item = products[indexPath.row]
        cell.image.image = UIImage()
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
            if let imageUrlString = item["galleryURL"]![0].string {
                if let imageUrl = NSURL(string: imageUrlString) {
                    if let imageData = NSData(contentsOfURL: imageUrl) {
                        if let image = UIImage(data: imageData) {
                            dispatch_async(dispatch_get_main_queue(), {
                                cell.image.image = image
                            })
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didUpdateFocusInContext context: UICollectionViewFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedIndexPath != nil {
            let item = products[context.nextFocusedIndexPath!.row]
            name.text = item["title"]![0].string
            if let pricing = item["sellingStatus"]![0]["convertedCurrentPrice"][0].dictionary {
                price.text = "\(pricing["__value__"]!.doubleValue)$"
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
