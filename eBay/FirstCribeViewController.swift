//
//  FirstCribeViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 16/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class FirstCribeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.reloadData()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Picked", name: "Screen", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Category.allCategorys.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CategoryCell", forIndexPath: indexPath) as! CategoryCollectionViewCell
        cell.imageView.image = UIImage(named: Category.allCategorys[indexPath.row].imageName)
        cell.category = Category.allCategorys[indexPath.row]
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let item = (appDelegate().manager.decisions[0]["categories"] as! [NSDictionary]).filter { cats -> Bool in
            cats["name"] as! String == Category.allCategorys[indexPath.row].title
        }.first as! [String : String]
        appDelegate().manager.last = item["id"]!
        appDelegate().manager.socket.emit("sendAnswer", ["cat_id" : item["id"]!, "stage" : item["lvl"]!])
    }
    
    func Picked() {
        self.performSegueWithIdentifier("PickedMain", sender: self)
    }

    func appDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
