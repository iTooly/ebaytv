//
//  TutorialControllerViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 14/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class TutorialViewController: UIPageViewController, UIPageViewControllerDataSource {
    
    var vcs : [TutorialPageViewController] = [TutorialPageViewController(), TutorialPageViewController(), TutorialPageViewController(), TutorialPageViewController()]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        for var i = 0; i < vcs.count; i++ {
            vcs[i].index = i
            vcs[i].imageView.image = UIImage(named: "tutorial_\(i + 2)")
        }
        
        self.setViewControllers([vcs[0]], direction: .Forward, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        switch (viewController as! TutorialPageViewController).index {
        case 1:
            return vcs[0]
        case 2:
            return vcs[1]
        case 3:
            return vcs[2]
        default:
            return nil
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        switch (viewController as! TutorialPageViewController).index {
        case 0:
            return vcs[1]
        case 1:
            return vcs[2]
        case 2:
            return vcs[3]
        default:
            return nil
        }
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return vcs.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return (pageViewController.viewControllers?.first as! TutorialPageViewController).index
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
