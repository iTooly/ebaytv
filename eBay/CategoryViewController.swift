//
//  CategoryViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 27/11/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController, UICollectionViewDataSource {
    
    var AdsCollection : UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        AdsCollection = UICollectionView(frame: view.frame, collectionViewLayout: UICollectionViewLayout())
        AdsCollection.dataSource = self
        
        self.view.addSubview(AdsCollection)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell(frame: CGRectMake(0, 0, 500, 500))
        cell.backgroundColor = .blackColor()
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}