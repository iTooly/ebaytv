//
//  FocusableButton.swift
//  eBay
//
//  Created by Gabriel Kapach on 14/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class FocusableButton: UIButton {

    var radius : CGFloat = 5
    var radiusF : CGFloat = 5
    var scale : CGFloat = 0.15
    var backColorF : Bool = true
    var tmpBackColor : UIColor?
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if self.focused {
            UIView.animateWithDuration(0.07, animations: { () -> Void in
                self.transform = CGAffineTransformMakeScale(1 + self.scale, 1 + self.scale)
                self.layer.shadowOffset = CGSizeMake(0, 5)
                self.layer.shadowRadius = 5
                self.layer.shadowOpacity = 0.5
                self.layer.cornerRadius = self.radiusF
                if self.backColorF {
                    self.tmpBackColor = self.backgroundColor
                    self.backgroundColor = UIColor(white: 0, alpha: 0.5)
                }
            })
        } else {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.transform = CGAffineTransformMakeScale(1, 1)
                self.layer.masksToBounds = true
                self.layer.shadowOffset = CGSizeMake(0, 0)
                self.layer.shadowRadius = 0
                self.layer.shadowOpacity = 0
                self.layer.cornerRadius = self.radius
                if self.backColorF {
                    self.backgroundColor = self.tmpBackColor
                }
            })
            
        }
        
    }

}
