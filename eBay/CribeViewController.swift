//
//  CribeHomeViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 05/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class CribeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!

    @IBOutlet weak var effect: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.reloadData()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "Picked", name: "Screen", object: nil)
        
        name.text = self.appDelegate().manager.product["title"]! as? String
        print(self.appDelegate().manager.product["galleryUrl"])
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
            print(-1)
            if let imageUrlString = self.appDelegate().manager.product["galleryUrl"] {
                print(0)
                if let imageUrl = NSURL(string: imageUrlString as! String) {
                    print(1)
                    if let imageData = NSData(contentsOfURL: imageUrl) {
                        print(2)
                        if let image = UIImage(data: imageData) {
                            print(3)
                            dispatch_async(dispatch_get_main_queue(), {
                                self.image.image = image
                                print(0)
                            })
                        }
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (appDelegate().manager.screen[0]["categories"]! as! [NSDictionary]).count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CategoryNameCell", forIndexPath: indexPath) as! CategoryNameCollectionViewCell
        
        cell.label.text = (appDelegate().manager.screen[0]["categories"]! as! [NSDictionary])[indexPath.row]["name"]! as? String
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        appDelegate().manager.socket.emit("sendAnswer", ["cat_id" : (appDelegate().manager.screen[0]["categories"]! as! [NSDictionary])[indexPath.row]["id"]!, "stage" : (appDelegate().manager.screen[0]["categories"]! as! [NSDictionary])[indexPath.row]["lvl"]!])
    }
    
    func collectionView(collectionView: UICollectionView, didUpdateFocusInContext context: UICollectionViewFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedIndexPath != nil {
            (collectionView.cellForItemAtIndexPath(context.nextFocusedIndexPath!) as! CategoryNameCollectionViewCell).label.textColor = .blackColor()
        }
        if context.previouslyFocusedIndexPath != nil {
            (collectionView.cellForItemAtIndexPath(context.previouslyFocusedIndexPath!) as! CategoryNameCollectionViewCell).label.textColor = .whiteColor()
        }
    }
    
    func Picked() {
        if (appDelegate().manager.screen[0]["categories"]! as! [NSDictionary]).count > 0 {
            collectionView.reloadData()
        } else {
            effect.hidden = false
            appDelegate().manager.socket.emit("sendAnswer", ["cat_id" : appDelegate().manager.last, "stage" : 1])
        }
    }

    func appDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
