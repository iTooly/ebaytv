//
//  LoginViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 30/11/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Login(sender: AnyObject) {
        let url = "http://178.62.123.100:8081/login/\(username.text!)/\(password.text!)".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        Alamofire.request(.GET, url).responseJSON { response in
            print(response.result.value)
            if response.result.isSuccess {
                if let json = JSON(response.result.value!).dictionary {
                    if json["success"]?.bool == true {
                        if let auth = json["auth_user"]?.string {
                            NSUserDefaults.standardUserDefaults().setValue(auth, forKey: "Auth")
                            print("Success")
                            self.performSegueWithIdentifier("Home", sender: self)
                        }
                    }
                }
            } else {
                print(String(data: response.data!, encoding: NSUTF8StringEncoding))
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
