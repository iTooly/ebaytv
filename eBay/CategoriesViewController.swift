//
//  CategoriesViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 27/11/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class CategoriesViewController: UITabBarController, UITabBarControllerDelegate {
    
    var Search : SearchViewController!
    var Home : HomeViewController!
    var Fashion : CategoryViewController = CategoryViewController()
    var Electronics : CategoryViewController = CategoryViewController()
    var Art : CategoryViewController = CategoryViewController()
    var Garden : CategoryViewController = CategoryViewController()
    var Login : LoginViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
        Search = self.viewControllers?[1] as! SearchViewController
        Home = self.viewControllers?.first as! HomeViewController
        Login = self.viewControllers?.last as! LoginViewController
        
        Search.tabBarItem = UITabBarItem(title: "Search", image: nil, tag: 0)
        Home.tabBarItem = UITabBarItem(title: "Home", image: nil, tag: 1)
        Fashion.tabBarItem = UITabBarItem(title: "Fashion", image: nil, tag: 2)
        Electronics.tabBarItem = UITabBarItem(title: "Electronics", image: nil, tag: 3)
        Art.tabBarItem = UITabBarItem(title: "Collectibles & Art", image: nil, tag: 4)
        Garden.tabBarItem = UITabBarItem(title: "Home & Garden", image: nil, tag: 5)
        Login.tabBarItem = UITabBarItem(title: "Login", image: nil, tag: 6)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.viewControllers = [Search, Home, Fashion, Electronics, Art, Garden, Login]
        self.selectedIndex = 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
