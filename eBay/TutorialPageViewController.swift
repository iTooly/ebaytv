//
//  TutorialPageViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 14/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class TutorialPageViewController: UIViewController {

    var imageView : UIImageView = UIImageView()
    var index : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.frame = self.view.frame
        self.view.addSubview(imageView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
