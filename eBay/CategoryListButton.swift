//
//  CategoryListButton.swift
//  eBay
//
//  Created by Gabriel Kapach on 05/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class CategoryListButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .whiteColor()
        self.setTitleColor(UIColor(red: 52 / 256, green: 170 / 256, blue: 220 / 256, alpha: 1), forState: UIControlState.Normal)
        self.alpha = 0.5
        self.titleLabel?.font = UIFont(name: "Roboto-Light", size: 30)
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        self.contentEdgeInsets = UIEdgeInsetsMake(0, 50, 0, 0)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if focused {
            self.alpha = 1
        } else {
            self.alpha = 0.5
        }
    }

}
