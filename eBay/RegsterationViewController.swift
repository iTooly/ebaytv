//
//  RegsterationViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 14/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegsterationViewController: UIViewController {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Register(sender: AnyObject) {
        Alamofire.request(.POST, "http://178.62.123.100:8081/register", parameters: ["name" : name.text!, "email" : email.text!, "password" : password.text!], encoding: .JSON, headers: nil).responseJSON { response -> Void in
            if response.result.isSuccess {
                if let data = JSON(response.result.value!).dictionary {
                    if data["success"]?.bool == true {
                        self.performSegueWithIdentifier("GoBackLogin", sender: self)
                    } else {
                        if let errors = data["errors"]?.array {
                            print(errors.description)
                        }
                    }
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
