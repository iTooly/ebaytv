/*
    Copyright (C) 2015 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    A struct used throughout the sample to represent example data.
*/

import Foundation

struct DataItem: Equatable {
    // MARK: Types
    
    enum Group: String {
        case BigAds
        case Categories
        
        static let allGroups: [Group] = [.BigAds, .Categories]
    }
    
    // MARK: Properties
    
    let group: Group
    
    let number: Int
    
    var title: String
    
    var identifier: String {
        return "\(group.rawValue).\(number)"
    }
    
    var imageURL: NSURL {
        let mainBundle = NSBundle.mainBundle()
        guard let imageURL = mainBundle.URLForResource(imageName, withExtension: nil) else { fatalError("Error determining local image URL.") }
        
        return imageURL
    }
}

// MARK: Equatable

func ==(lhs: DataItem, rhs: DataItem)-> Bool {
    // Two `DataItem`s are considered equal if their identifiers match.
    return lhs.identifier == rhs.identifier
}
