//
//  CategoryCollectionViewCell.swift
//  eBay
//
//  Created by Gabriel Kapach on 05/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    var category: Category?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // These properties are also exposed in Interface Builder.
        imageView.adjustsImageWhenAncestorFocused = true
        imageView.clipsToBounds = false
    }

}
