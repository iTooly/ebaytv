//
//  QuestionListButton.swift
//  eBay
//
//  Created by Gabriel Kapach on 05/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class QuestionListLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(red: 52 / 256, green: 170 / 256, blue: 220 / 256, alpha: 1)
        self.textColor = .whiteColor()
        self.font = UIFont(name: "Roboto-Medium", size: 32)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func drawTextInRect(rect: CGRect) {
        return super.drawTextInRect(UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(0, 50, 0, 0)))
    }

}
