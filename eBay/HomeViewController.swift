//
//  HomeViewController.swift
//  eBay
//
//  Created by Gabriel Kapach on 01/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class HomeViewController: CollectionViewContainerViewController {
    
    var MenuBar : UIViewController!
    let launchCounter = NSUserDefaults.standardUserDefaults().integerForKey("launches")

    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        MenuBar = self.storyboard?.instantiateViewControllerWithIdentifier("HomeMenuBar")
        MenuBar.view.frame = CGRectMake(90, 60, 1740, 75)
        self.view.addSubview(MenuBar.view)
        self.addChildViewController(MenuBar)
        MenuBar.didMoveToParentViewController(self)
        */
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "moveToGame", name: "GoHomeGame", object: nil)

    }
    
    override func viewDidAppear(animated: Bool) {
        CribePopup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    }
    
    func moveToGame() {
        self.performSegueWithIdentifier("GoHomeGame", sender: self)
    }
    
    func CribePopup() {
        if (UIApplication.sharedApplication().delegate as! AppDelegate).cribePopupF {
            (UIApplication.sharedApplication().delegate as! AppDelegate).cribePopupF = false
            let popup = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("CribePopup")
            self.addChildViewController(popup)
            popup.didMoveToParentViewController(self)
            popup.view.alpha = 0
            popup.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            popup.view.center = self.view.center
            self.view.addSubview(popup.view)
            UIView.animateWithDuration(0.3, delay: 3, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                popup.view.transform = CGAffineTransformMakeScale(1, 1)
                popup.view.alpha = 1
                }, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
