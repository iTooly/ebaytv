//
//  GameManager.swift
//  eBay
//
//  Created by Gabriel Kapach on 16/12/2015.
//  Copyright © 2015 eBay. All rights reserved.
//

import UIKit

class GameManager: NSObject {
    
    var socket : SocketIOClient!
    var decisions : NSArray!
    var screen : NSArray!
    var product : NSDictionary!
    var last : String!
    
    init(instance: SocketIOClient) {
        super.init()
        socket = instance
        
        socket.on("connect") { (data, ack) -> Void in
            self.appDelegate().manager.socket.emit("getDecisions", [])
        }
        
        socket.on("Screen*1") { (data, ack) -> Void in
            if data![0]["success"] as! Int == 1 {
                self.decisions = data
                NSNotificationCenter.defaultCenter().postNotificationName("DecisionsArrived", object: nil)
            }
        }
        
        socket.on("Screens") { (data, ack) -> Void in
            if data![0]["success"] as! Int == 1 {
                self.screen = data
                NSNotificationCenter.defaultCenter().postNotificationName("Screen", object: nil)
            }
        }
        
        socket.on("product") { (data, ack) -> Void in
            
            self.product = data![0] as! NSDictionary

        }
        
        socket.connect()
    }
    
    func appDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
}
